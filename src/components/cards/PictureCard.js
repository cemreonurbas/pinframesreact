import { Col, Row, Space } from "antd";
import React, { Component } from "react";

const fileToDataUri = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (event) => {
      resolve(event.target.result);
    };
    reader.readAsDataURL(file);
  });

export default class PictureCard extends Component {
  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);

    fileToDataUri(props.data.sourceInfo.file).then((dataUri) => {
      this.setState({ dataUri: dataUri });
    });

    this.state = {
      isHovering: false,
      dataUri: null,
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering,
    };
  }

  render() {
    return (
      <Col span={8}>
        <div
          style={{
            width: 240,
            height: 240,
            backgroundImage: "url(../TileClassic.png)",
            backgroundRepeat: "no-repeat",
            backgroundSize: 240,
          }}
        >
          <div
            className="imagePic"
            onMouseEnter={this.handleMouseHover}
            onMouseLeave={this.handleMouseHover}
          >
            <img
              alt="pinframes"
              style={{
                width: "inherit",
                position: "absolute",
                top: 0,
                bottom: 0,
                margin: "auto",
              }}
              src={this.state.dataUri}
            />
            {this.state.isHovering && (
              <Row
                justify="end"
                style={{ position: "absolute", top: 8, right: 8 }}
              >
                <Space direction="vertical" wrap>
                  <div
                    style={{
                      width: 32,
                      height: 32,
                      background: "white",
                      borderRadius: 4,
                      boxShadow: " 0px 0px 4px rgba(0, 0, 0, 0.5)",
                      textAlign: "center",
                    }}
                    onClick={this.props.onCropEvent}
                  >
                    <img alt="pinframes" src="../crop.png" />
                  </div>
                  <div
                    style={{
                      width: 32,
                      height: 32,
                      background: "white",
                      borderRadius: 4,
                      boxShadow: " 0px 0px 4px rgba(0, 0, 0, 0.5)",
                      textAlign: "center",
                    }}
                    onClick={this.props.onDeleteEvent}
                  >
                    <img alt="pinframes" src="../delete.png" />
                  </div>
                </Space>
              </Row>
            )}
          </div>
        </div>
      </Col>
    );
  }
}
