import React, { useRef } from "react";
import { Widget } from "@uploadcare/react-widget";
import strings from "../../helpers/Localization";
import { Button } from "antd";
const PhotoButton = ({ onClick }) => {
  const widgetApi = useRef();

  const fileSelect = (file) => {
    console.log("File changed: ", file);

    if (file) {
      file.progress((info) => console.log("File progress: ", info.progress));
      file.done((info) => {
        console.log("File uploaded: ", info);
        onClick(info);
      });
    }
  };
  return (
    <>
      <div style={{ display: "none" }}>
        <Widget
          ref={widgetApi}
          tabs="file"
          publicKey="demopublickey"
          onFileSelect={fileSelect}
          onChange={(info) => console.log("Upload completed:", info)}
        />
      </div>
      <Button
        onClick={() => widgetApi.current.openDialog()}
        className="actionbutton"
        style={{ marginBottom: 25 }}
        type="primary"
      >
        {strings.load_photo}
      </Button>
    </>
  );
};

export default PhotoButton;
