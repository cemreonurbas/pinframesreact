import BlogItem from "../models/BlogItem";

const PINFRAMES_ROOT = "https://pinframes.com";
const privacyPolicy = "/mobile/doc/gizlilik-sozlemesi";
const termsOfUse = "/mobile/doc/kullanici-sozlesmesi";
const orderAgreement = "/mobile/doc/mesafeli-satis-sozlesmesi";
const preAgreement = "/mobile/doc/onbilgiler";
const blogArray = [
  new BlogItem({
    blogImage: "../blogs/blog_images/1.jpg",
    blogId: 1,
    blogTitle: "PinFrames Çerçeveleri ile Sanat Eserleri",
    blogSubtitle:
      "Lord Aurbery’nin de dediği gibi söze başlamak hoş olacaktır; ‘Güneşin çiçekleri renklendirmesi gibi sanat da hayata renk..",
  }),
  new BlogItem({
    blogImage: "../blogs/blog_images/2.jpg",
    blogId: 2,
    blogTitle: "PinFrames Duvar Dekorasyonu",
    blogSubtitle:
      "Hayatımızın büyük birçoğunu duvarlarla ayrılmış odalarda geçiririz. Bu duvarlar bazen evimizde bazen de ofisimizde..",
  }),
  new BlogItem({
    blogImage: "../blogs/blog_images/3.jpg",
    blogId: 3,
    blogTitle: "Kare Çerçeveler için Fotoğraf Nasıl Çekilir?",
    blogSubtitle:
      "En mutlu, heyecanlı olduğumuz, o anın hiç bitmesini istemediğimiz anlarda elimiz telefonlarımıza, fotoğraf..",
  }),
  new BlogItem({
    blogImage: "../blogs/blog_images/4.jpg",
    blogId: 4,
    blogTitle: "Minimalist Duvarların Adresi PinFrames",
    blogSubtitle:
      "Minimalizmden hoşlanıyor ve hayatınızdaki fazlalıklardan, sizi yoran ve ihtiyacınız olmayan detaylardan..",
  }),
  new BlogItem({
    blogImage: "../blogs/blog_images/5.jpg",
    blogId: 5,
    blogTitle: "PinFrames Çerçeveleri ile Sanat Eserleri",
    blogSubtitle:
      "Sevdiklerimizle güldüğümüz eğlendiğimiz anlar, bebeğimizin ilk adımlarında, ilk limonla tanıştığı anlar, okula..",
  }),
];

export {
  PINFRAMES_ROOT,
  privacyPolicy,
  termsOfUse,
  orderAgreement,
  preAgreement,
  blogArray,
};
